import logo from './logo.svg';
import './App.css';
import React,{useState} from 'react'
const parser = require('js-sql-parser');

function App() {
  const [valueData, setValueData] = useState('3*2+1');
  const hundleClick = () => {
    try {
      const ast = parser.parse(valueData);
      console.log('ast',ast)
    } catch (e) {
    console.log('Error',e)
    }
  }


  return (
    <div className="App">
      <header className="App-header">
        <textarea value={valueData}>
        </textarea>
        <button onClick={hundleClick}>Click</button>
      </header>
    </div>
  );
}

export default App;
